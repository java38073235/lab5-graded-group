package shapes;
//Anthony Relae
public class Cylinder implements Shape3d {
    
    private double height;
    private double radius;

    public Cylinder(double height, double radius){
        if(height<=0 || radius<=0)
        {
            throw new IllegalArgumentException("input not valid");
        }
        this.height=height;
        this.radius=radius;
    }

    public double getVolume(){
        return Math.PI*(this.radius*this.radius)*this.height;
        // throw new UnsupportedOperationException("Not written yet");
    }

    public double getSurfaceArea(){

        return 4*this.radius+2*height;

        // throw new UnsupportedOperationException("Not written yet");

    }

    public double getRadius(){
        return this.radius;
        // throw new UnsupportedOperationException("Not written yet");

    }

    public double getHeight(){
        return this.height;
        // throw new UnsupportedOperationException("Not written yet");

    }

    public String toString(){
        return "the height is " + this.height+" the radius is "+this.radius;
        // throw new UnsupportedOperationException("Not written yet");

    }

}
