package shapes;

public class Sphere implements Shape3d{
    private double radius;

    public Sphere(double radius){
        if(radius < 0){
            throw new IllegalArgumentException("Radius must be higher than 0");
        }
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    //volume = 3/4*Math.PI*radius*radius*radius
    public double getVolume(){
        return (Math.PI * (Math.pow(this.radius, 3)));
        //throw new UnsupportedOperationException("nothing written yet!");
    }
    //return Surface Area
    public double getSurfaceArea(){
        return (4 * Math.PI * (Math.pow(this.radius, 2)));
    }
}