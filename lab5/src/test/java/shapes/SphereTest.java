package shapes;

import static org.junit.Assert.*;

import org.junit.Test;

public class SphereTest {
    
    
    @Test
    public void testingConstructer()
    {
        try{
        Sphere s = new Sphere(-5);
        fail("Radius was negative, but constructor allowed it.");
        }
        catch(Exception e){
            //pass!
        }
    }

    @Test
    public void testingGetVolume()
    {
        Sphere s = new Sphere(5);
        assertEquals("Volume should be ",s.getVolume(), 392.6990816987241,0);
    }

    @Test
    public void testSurfaceArea()
    {
        Sphere s= new Sphere(5);
        assertEquals("surphace area is incorrect",s.getSurfaceArea(),314.1592653589793, 0);
    }

    @Test
    public void testRadius()
    {
        Sphere s=new Sphere(5);
        assertEquals("radius is supposed to be 5", s.getRadius(), 5, 0);
    }
}
