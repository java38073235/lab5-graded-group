//Luis Thésée
package shapes;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UnitTesting{

    @Test
    public void testConstructor(){
        Cylinder c = new Cylinder(5, 2);
        assertEquals("radius should be 2", c.getRadius(), 2, 0);
        assertEquals("height should be 5", c.getHeight(), 5, 0);
    }

    @Test
    public void testVolume(){
        Cylinder c = new Cylinder(2, 3);
        assertEquals("Volume should be 56.54", c.getVolume(), 56.54, 0.01);
    }

    @Test
    public void testSurfaceArea(){
        Cylinder c = new Cylinder(2, 5);
        assertEquals("Area should be 24", c.getSurfaceArea(), 24, 0);
    }
}